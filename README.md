# DA — Менеджмент баз та сховищ данних — Курсова робота

### Стиснення дампу бази даних
```shell
tar cvzf - <filename>.sql | split --bytes=90MB - <filename>.sql.tar.gz.
```

### Розпаковка архіву з дампом бази даних
```shell
cat <filename>.sql.tar.gz.* | tar xzvf -
```

### Завантаження набору даних в СУБД MariaDB/MySQL (OS: Ubuntu Linux, User: root)
```shell
mysql <db_name> < <filename>.sql
```
